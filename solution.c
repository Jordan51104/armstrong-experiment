#include <stdbool.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>

// basic steps
// make function that turns an int into an array of its place values


int findLength(int num) {
	int length = 0;
	int divisor = 1;
	while (num / divisor >= 1) {
		length++;
		divisor *= 10;
	}
	return length;
}

int* makePlaceValueArray(int num) {
	int lengthOfNum = findLength(num);
	int copyOfNum = num;
	int *placeValueArray = malloc(lengthOfNum*sizeof(int));
	for (int i = 0; i < lengthOfNum; i++) {
		int exponent = lengthOfNum-(i+1);
		int value = copyOfNum/pow(10, exponent);
		copyOfNum -= ((value) * pow(10, exponent));
		placeValueArray[i] = value;
	}
	return placeValueArray;
}
int findArmstrong(int* num, int length) {
	int collector = 0;
	for (int i = 0; i < length; i++) {
		collector += (int)pow(num[i], length);
	}
	return collector;
}

bool isArmstrong(int num) {
	int* placeValueArray = makePlaceValueArray(num);
	if(num == findArmstrong(placeValueArray, findLength(num))) {
		return true;
	}
	free(placeValueArray);
	return false;
}

int main() {
	int armstrongs = 0;
	int armstrong_numbers[250];
	int i = 0;
	int x = 0;
	while(armstrongs <= 250) {
		i++;
		if (isArmstrong(i)) {
			if (armstrongs % 5 == 0) {
				printf("i: %d isArmstrong: %d armstrongs: %d\n", i, isArmstrong(i), armstrongs);
			}
			armstrongs++;
			armstrong_numbers[x] = i;
			x++;
		}
	}
	for(int i = 0; i < armstrongs; i++) {
		printf("%d, ", armstrong_numbers[i]);
	}
}
